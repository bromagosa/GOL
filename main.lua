local sideSize = 30
local cellSize = 5
local board = {}
local buffer = {}

function initialize()
    local lines = {}
    local x = 0
    for line in io.lines() do
        x = x + 1
        sideSize = string.len(line)
        board[x] = {}
        for y = 1, sideSize do
            local c = tonumber(string.sub(line, y, y))
            board[x][y] = c
        end
    end
    clearBuffer()
end

function clearBuffer()
    for x = 1, sideSize do
        buffer[x] = {}
    end
end

function cellAt(x, y)
    return board[x][y]
end

function aliveAt(x, y)
    return cellAt(x, y) == 1
end

function amountOfAliveNeighborsAt(x, y)
    local amount = 0

    if x > 1 then 
        amount = amount + cellAt(x - 1, y)
        if y < sideSize then amount = amount + cellAt(x - 1, y + 1) end
        if y > 1 then amount = amount + cellAt(x - 1, y - 1) end
    end

    if y > 1 then
        amount = amount + cellAt(x, y - 1)
        if x < sideSize then amount = amount + cellAt(x + 1, y - 1) end
    end

    if x < sideSize then 
        amount = amount + cellAt(x + 1, y)
        if y < sideSize then amount = amount + cellAt(x + 1, y + 1) end
    end

    if y < sideSize then amount = amount + cellAt(x, y+1) end

    return amount
end

function tickAt(x, y)
    local aliveOnes = amountOfAliveNeighborsAt(x, y)
    if aliveAt(x, y) and (aliveOnes < 2 or aliveOnes > 3) then
        buffer[x][y] = 0
    elseif not aliveAt(x, y) and aliveOnes == 3 then
        buffer[x][y] = 1
    end
end

function updateBoard()
    for x, row in pairs(buffer) do
        for y, cell in pairs(row) do
            board[x][y] = buffer[x][y]
        end
    end
end

function tick()
    for x = 1, sideSize do
        for y = 1, sideSize do
            tickAt(x, y)
        end
    end
    updateBoard()
    clearBuffer()
end

function love.load()
    initialize()
end

function love.update(dt)
    tick()
end

function love.draw()
    for y, col in pairs(board) do
        for x, element in pairs(col) do
            if element == 1 then
                love.graphics.setColor(255, 255, 255);
            else
                love.graphics.setColor(0, 0, 0);
            end
            love.graphics.rectangle('fill', x * cellSize, y * cellSize, cellSize, cellSize)
        end
    end
end
