## Conway's Game of Life

This is my toy project to get started in Lua.

Put these files in a directory (GOL, for instance) and run:

    love GOL < GOL/board

The board file describes a Game of Life initial seed. Edit it at your will.

![Screenshot](shot.png)
